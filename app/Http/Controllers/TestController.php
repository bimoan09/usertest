<?php

namespace App\Http\Controllers;

use App\UserTest;
use Illuminate\Http\Request;

class TestController extends Controller
{

    // public function __construct(UserTest $id)
    // {
    //      $this->id;
    // }

    public function index()
    {
        $usertest = UserTest::get();
        return view('index', compact('usertest'));
    }

    public function store(Request $request)
    {

        // ambil id terakhir dari UserTest, ID terakhir = ID paling besar 
        $prevId = UserTest::max('id');
     
        $usertest = new UserTest;
        $usertest->name = $request->name;
        if( $prevId != null) {
        $prevId++;
        $usertest->parity = ($prevId % 2 == 0) ? 'Even' : 'Odd';
        }
        else{
        // jika tidak ada data rows masih NULL, maka ketika pertama di input ID nya 1 , 1 = ganjil (Odd)
         $usertest->parity = 'Odd';
        };

        $usertest->save();
        return redirect()->route('index');
    }

}
